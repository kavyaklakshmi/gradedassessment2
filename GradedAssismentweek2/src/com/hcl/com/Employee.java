package com.hcl.com;

public class Employee {int id;
String name;
int age;
double salary; 
String department;
String city;
public Employee() {
	super();
}

public Employee(int id, String name, int age, double salary, String department, String city){
	super();
	

    this.id = id;

    
    this.name = name;

   
    this.age = age;

    
    this.salary = salary;

    
    this.department = department;

    
    this.city = city;

} 


public int getId() {
	return id;
}
public void setId(int id) {
    this.id = id;
}
public String getName() {
    return name;
}
public void setName(String name) {
    this.name = name;
}
public int getAge() {
    return age;
}
public void setAge(int age) {
    this.age = age;
}
public double getSalary() {
    return salary;
}
public void setSalary(double salary) {
    this.salary = salary;
}
public String getDepartment() {
    return department;
}
public void setDepartment(String department) {
    this.department = department;
}
public String getCity() {
    return city;
}
public void setCity(String city) {
    this.city = city;
}


@Override
	public String toString() {
    return "  "+id +"\t"+name+"\t  "+age+"\t"+salary+"\t\t  "+department+"\t\t"+city;
}


}
