package com.hcl.com;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class DataStructureB {
	
		public void cityNameCount(ArrayList<Employee> employees) {
			Set<String> setList = new TreeSet<String>();
	        ArrayList<String> arrayList = new ArrayList<String>();

	        for (Employee emp : employees) {
	           	arrayList.add(emp.getCity());
	            setList.add(emp.getCity());
	        }
	        
	        System.out.println("Count of Employees from each City:");
	        System.out.print("{ ");
	        for (String city_name : setList) {
	            System.out.print(city_name + "=" + Collections.frequency(arrayList, city_name) + " ");
	        }
	        System.out.println("}");
	        
	     }

	     public void monthlySalary(ArrayList<Employee> employees) {
	    	 System.out.println("Monthly Salary of employee along with their ID: ");
	    	 System.out.print("{ ");
	         for (Employee emp1 : employees) {
	              System.out.print(emp1.getId() +"=" + emp1.getSalary() / 12 + " ");
	         }
	         System.out.println("}");
	     }

	}



